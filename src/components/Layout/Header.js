import { useContext } from "react";
import CollectionsContext from "../../stores/collections-context";
import classes from "./Header.module.css";

const Header = () => {
  const collectionsContext = useContext(CollectionsContext);

  const showCollectionsBtnClickHandler = () => {
    collectionsContext.showCollection();
  };

  return (
    <header className={classes.header}>
      <div className={classes.header__left}>
        <button onClick={showCollectionsBtnClickHandler}>
          <i id="showCollectionsBtn" className="fa fa-bars"></i>
        </button>
        <div className={classes.header__title}>
          <i className="fa fa-list-alt" />
          <span>Dashboard</span>
        </div>
      </div>
      <div className={classes.header__right}>
        <i className="fa fa-user" />
      </div>
    </header>
  );
};

export default Header;
