import classes from "./ItemButtonsGroup.module.css";

const ItemButtonsGroup = (props) => {
  const deleteButtonClickHandler = (event) => {
    event.stopPropagation();
    props.onDeleteButtonClick();
  };

  const editButtonClickHandler = (event) => {
    event.stopPropagation();
    props.onEditButtonClick();
  };

  return (
    <span className={classes.buttons__group}>
      <button onClick={deleteButtonClickHandler}>
        <i className="fa fa-times-circle"></i>
      </button>
      {!props.isComplete && (
        <button onClick={editButtonClickHandler}>
          <i className="fa fa-edit"></i>
        </button>
      )}
    </span>
  );
};

export default ItemButtonsGroup;
