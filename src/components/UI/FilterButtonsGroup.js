import classes from "./FilterButtonsGroup.module.css";

const FilterButtonsGroup = (props) => {
  return (
    <div className={classes.buttons__group}>
      <button onClick={props.onAscendingButtonClick}>
        <i data-list="task" className="sortBtn fa fa-sort-alpha-up" />
      </button>
      <button onClick={props.onDescendingButtonClick}>
        <i data-list="task" className="sortBtn fa fa-sort-alpha-down" />
      </button>
    </div>
  );
};

export default FilterButtonsGroup;
