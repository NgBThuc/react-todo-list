import { forwardRef } from "react";
import classes from "./Input.module.css";

const Input = forwardRef((props, ref) => {
  const inputClasses = `${classes.input} ${classes[props.className]}`;

  return (
    <form onSubmit={props.onSubmit} className={inputClasses}>
      <button type="submit">
        <i id="addTaskBtn" className="fa fa-plus" />
      </button>
      <input ref={ref} type="text" placeholder="Add a task" />
    </form>
  );
});

export default Input;
