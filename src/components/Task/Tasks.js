import { nanoid } from "nanoid";
import { useContext, useRef } from "react";
import CollectionsContext from "../../stores/collections-context";
import Input from "../UI/Input";
import classes from "./Tasks.module.css";
import TasksDetail from "./TasksDetail";
import TasksHeader from "./TasksHeader";

const Tasks = () => {
  const collectionsContext = useContext(CollectionsContext);
  const addTaskInputRef = useRef();

  const todoList = collectionsContext.currentCollection.tasksList.filter(
    (taskItem) => taskItem.isComplete === false
  );

  const completeList = collectionsContext.currentCollection.tasksList.filter(
    (taskItem) => taskItem.isComplete === true
  );

  const addTaskHandler = (event) => {
    event.preventDefault();

    if (addTaskInputRef.current.value.trim().length <= 0) {
      addTaskInputRef.current.value = "";
      return;
    }

    let newTaskContent = addTaskInputRef.current.value;
    addTaskInputRef.current.value = "";
    collectionsContext.addTask({
      id: nanoid(),
      content: newTaskContent,
      isComplete: false,
    });
  };

  return (
    <div className={classes.tasks}>
      <TasksHeader />
      <Input
        ref={addTaskInputRef}
        onSubmit={addTaskHandler}
        className="tasks__input"
      />
      <TasksDetail key="todo" taskType="todo" tasksList={todoList} />
      <TasksDetail
        key="incomplete"
        taskType="incomplete"
        tasksList={completeList}
      />
    </div>
  );
};

export default Tasks;
