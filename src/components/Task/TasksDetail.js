import { useEffect, useState } from "react";
import classes from "./TasksDetail.module.css";
import TasksList from "./TasksList";
import TasksTitle from "./TasksTitle";

const TasksDetail = (props) => {
  const [tasksList, setTasksList] = useState(props.tasksList);

  const ascendingSortHandler = () => {
    let sortedTasksList = [...tasksList].sort((a, b) =>
      a.content.localeCompare(b.content)
    );
    setTasksList(sortedTasksList);
  };

  const descendingSortHandler = () => {
    let sortedTasksList = [...tasksList].sort((a, b) =>
      b.content.localeCompare(a.content)
    );
    setTasksList(sortedTasksList);
  };

  useEffect(() => {
    setTasksList(props.tasksList);
  }, [props.tasksList]);

  return (
    <div className={classes.tasks__detail}>
      <TasksTitle
        ascendingSortHandler={ascendingSortHandler}
        descendingSortHandler={descendingSortHandler}
        tasksList={tasksList}
        taskType={props.taskType}
      />
      <TasksList tasksList={tasksList} />
    </div>
  );
};

export default TasksDetail;
