import { useContext, useEffect, useRef, useState } from "react";
import CollectionsContext from "../../stores/collections-context";
import ItemButtonsGroup from "../UI/ItemButtonsGroup";
import classes from "./TasksItem.module.css";

const TasksItem = (props) => {
  const [isEdit, setIsEdit] = useState(false);
  const collectionsContext = useContext(CollectionsContext);
  const taskContentInputRef = useRef();
  const taskContentInputEl = taskContentInputRef.current;

  const taskEditHandler = () => {
    setIsEdit(true);
  };

  const taskEditBlurHandler = () => {
    taskContentInputEl.scrollLeft = "0px";
    setIsEdit(false);
    if (taskContentInputEl.innerText.trim().length > 0) {
      collectionsContext.adjustTask(
        props.id,
        taskContentInputEl.innerText.trim()
      );
    } else {
      taskContentInputEl.innerText = props.content;
    }
  };

  const taskEditKeyDownHandler = (event) => {
    if (event.key === "Enter") {
      taskContentInputEl.scrollLeft = "0px";
      setIsEdit(false);
      if (taskContentInputEl.innerText.trim().length > 0) {
        collectionsContext.adjustTask(
          props.id,
          taskContentInputEl.innerText.trim()
        );
      } else {
        taskContentInputEl.innerText = props.content;
      }
    }
    if (event.key === "Escape") {
      taskContentInputEl.scrollLeft = "0px";
      setIsEdit(false);
      taskContentInputEl.innerText = props.content;
    }
  };

  const checkBoxChangeHandler = () => {
    collectionsContext.toggleTask(props.id);
  };

  const taskDeleteHandler = () => {
    collectionsContext.deleteTask(props.id);
  };

  useEffect(() => {
    if (taskContentInputEl && isEdit) {
      taskContentInputEl.focus();
      window.getSelection().selectAllChildren(taskContentInputEl);
    }
  }, [taskContentInputEl, isEdit]);

  return (
    <div className={classes.item}>
      <div>
        <input
          checked={props.isComplete}
          id={props.id}
          type="checkbox"
          onChange={checkBoxChangeHandler}
        />
        <label htmlFor={props.id} className={classes.checkIcon}>
          <i className="fa fa-check" />
        </label>
      </div>
      <div className={classes.detail}>
        <span
          contentEditable={isEdit}
          suppressContentEditableWarning={true}
          ref={taskContentInputRef}
          onBlur={taskEditBlurHandler}
          onKeyDown={taskEditKeyDownHandler}
          title={props.content}
        >
          {props.content}
        </span>
      </div>
      <ItemButtonsGroup
        isComplete={props.isComplete}
        onDeleteButtonClick={taskDeleteHandler}
        onEditButtonClick={taskEditHandler}
      />
    </div>
  );
};

export default TasksItem;
