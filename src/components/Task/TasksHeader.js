import { useContext } from "react";
import CollectionsContext from "../../stores/collections-context";
import classes from "./TasksHeader.module.css";

const TasksHeader = () => {
  const collectionsContext = useContext(CollectionsContext);

  const backCollectionClickHandler = () => {
    collectionsContext.backToPrevCollection();
  };

  return (
    <div className={classes.tasks__header}>
      <button
        disabled={collectionsContext.collectionsStack <= 0}
        onClick={backCollectionClickHandler}
      >
        <i className="fa fa-chevron-left" />
      </button>
      <h2 title={collectionsContext.currentCollection.name}>
        {collectionsContext.currentCollection.name}
      </h2>
    </div>
  );
};

export default TasksHeader;
