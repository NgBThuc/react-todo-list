import FilterButtonsGroup from "../UI/FilterButtonsGroup";
import classes from "./TasksTitle.module.css";

const TasksTitle = (props) => {
  return (
    <div className={classes.tasks__title}>
      <h3>
        {props.taskType === "todo" ? "Task" : "Completed"} -{" "}
        <span>{props.tasksList.length}</span>
      </h3>
      <FilterButtonsGroup
        taskType={props.taskType}
        onAscendingButtonClick={props.ascendingSortHandler}
        onDescendingButtonClick={props.descendingSortHandler}
      />
    </div>
  );
};

export default TasksTitle;
