import TasksItem from "./TasksItem";
import classes from "./TasksList.module.css";

const TasksList = (props) => {
  const tasksList = props.tasksList.map((taskItem) => (
    <TasksItem
      key={taskItem.id}
      id={taskItem.id}
      content={taskItem.content}
      isComplete={taskItem.isComplete}
    />
  ));

  return <div className={classes.tasks__list}>{tasksList}</div>;
};

export default TasksList;
