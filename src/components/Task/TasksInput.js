import classes from "./TasksInput.module.css";

const TasksInput = () => {
  return (
    <div className={classes.tasks__input}>
      <label htmlFor="addTaskInput">
        <i id="addTaskBtn" className="fa fa-plus" />
      </label>
      <input id="addTaskInput" type="text" placeholder="Add a task" />
    </div>
  );
};

export default TasksInput;
