import { useContext } from "react";
import CollectionsContext from "../../stores/collections-context";
import CollectionsItem from "./CollectionsItem";
import classes from "./CollectionsList.module.css";

const CollectionsList = () => {
  const collectionsContext = useContext(CollectionsContext);

  return (
    <div className={classes.collections__list}>
      {collectionsContext.collectionsList.map((collection, index) => {
        return (
          <CollectionsItem
            key={collection.id}
            id={collection.id}
            name={collection.name}
          />
        );
      })}
    </div>
  );
};

export default CollectionsList;
