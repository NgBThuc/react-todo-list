import { useContext } from "react";
import CollectionsContext from "../../stores/collections-context";
import classes from "./CollectionsHeader.module.css";

const CollectionsHeader = () => {
  const collectionsContext = useContext(CollectionsContext);

  const hideCollectionsBtnClickHandler = () => {
    collectionsContext.hideCollection();
  };

  return (
    <div className={classes.collections__header}>
      <h2>Collections</h2>
      <button onClick={hideCollectionsBtnClickHandler}>
        <i className="fa fa-angle-double-left"></i>
      </button>
    </div>
  );
};

export default CollectionsHeader;
