import { useContext, useEffect, useRef, useState } from "react";
import CollectionsContext from "../../stores/collections-context";
import ItemButtonsGroup from "../UI/ItemButtonsGroup";
import classes from "./CollectionsItem.module.css";

const CollectionsItem = (props) => {
  const [isEdit, setIsEdit] = useState(false);
  const collectionsContext = useContext(CollectionsContext);
  const collectionNameInputRef = useRef();
  const collectionNameInputEl = collectionNameInputRef.current;

  const collectionRemoveHandler = () => {
    collectionsContext.removeCollection(props.id);
  };

  const collectionSelectHandler = () => {
    collectionsContext.selectCollection(props.id);
    collectionsContext.hideCollection();
  };

  const collectionEditHandler = () => {
    setIsEdit(true);
  };

  const collectionEditBlurHandler = () => {
    collectionNameInputEl.scrollLeft = "0px";
    setIsEdit(false);
    if (collectionNameInputEl.innerText.trim().length > 0) {
      collectionsContext.adjustCollection(
        props.id,
        collectionNameInputEl.innerText.trim()
      );
    } else {
      collectionNameInputEl.innerText = props.name;
    }
  };

  const collectionEditKeyDownHandler = (event) => {
    if (event.key === "Enter") {
      collectionNameInputEl.scrollLeft = "0px";
      setIsEdit(false);
      if (collectionNameInputEl.innerText.trim().length > 0) {
        collectionsContext.adjustCollection(
          props.id,
          collectionNameInputEl.innerText.trim()
        );
      } else {
        collectionNameInputEl.innerText = props.name;
      }
    }

    if (event.key === "Escape") {
      collectionNameInputEl.scrollLeft = "0px";
      setIsEdit(false);
      collectionNameInputEl.innerText = props.name;
    }
  };

  useEffect(() => {
    if (collectionNameInputEl && isEdit) {
      collectionNameInputEl.focus();
      window.getSelection().selectAllChildren(collectionNameInputEl);
    }
  }, [collectionNameInputEl, isEdit]);

  return (
    <div
      onClick={collectionSelectHandler}
      className={classes.collections__item}
    >
      <span
        className={classes.collections__name}
        contentEditable={isEdit}
        suppressContentEditableWarning="true"
        ref={collectionNameInputRef}
        onBlur={collectionEditBlurHandler}
        onKeyDown={collectionEditKeyDownHandler}
      >
        {props.name}
      </span>
      <ItemButtonsGroup
        onDeleteButtonClick={collectionRemoveHandler}
        onEditButtonClick={collectionEditHandler}
        id={props.id}
      />
    </div>
  );
};

export default CollectionsItem;
