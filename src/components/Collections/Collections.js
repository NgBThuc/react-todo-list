import { nanoid } from "nanoid";
import { useContext, useRef } from "react";
import CollectionsContext from "../../stores/collections-context";
import Input from "../UI/Input";
import classes from "./Collections.module.css";
import CollectionsHeader from "./CollectionsHeader";
import CollectionsList from "./CollectionsList";

const Collections = () => {
  const collectionInputRef = useRef();
  const collectionsContext = useContext(CollectionsContext);

  const collectionsClasses = `${classes.collections} ${
    collectionsContext.isCollectionShow && classes.show
  }`;

  const submitHandler = (event) => {
    event.preventDefault();

    if (collectionInputRef.current.value.trim().length <= 0) {
      collectionInputRef.current.value = "";
      return;
    }

    let newCollectionsName = collectionInputRef.current.value;
    collectionInputRef.current.value = "";
    collectionsContext.addCollection({
      id: nanoid(),
      name: newCollectionsName,
      tasksList: [],
    });
  };

  return (
    <div className={collectionsClasses}>
      <CollectionsHeader />
      <CollectionsList />
      <Input
        ref={collectionInputRef}
        onSubmit={submitHandler}
        className="collections__input"
      />
    </div>
  );
};

export default Collections;
