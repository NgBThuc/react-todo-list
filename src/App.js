import { useContext } from "react";
import "./App.css";
import Collections from "./components/Collections/Collections";
import Container from "./components/Layout/Container";
import Header from "./components/Layout/Header";
import Tasks from "./components/Task/Tasks";
import CollectionsContext from "./stores/collections-context";

function App() {
  const collectionsContext = useContext(CollectionsContext);

  return (
    <Container>
      <Header />
      <Collections />
      {Object.keys(collectionsContext.currentCollection).length !== 0 && (
        <Tasks />
      )}
    </Container>
  );
}

export default App;
