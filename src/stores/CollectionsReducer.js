const saveCollectionsToLocalStorage = (collectionsList) => {
  localStorage.setItem(
    "LOCALSTORAGE_COLLECTIONS",
    JSON.stringify(collectionsList)
  );
};

export const initialCollectionsState = {
  collectionsList:
    JSON.parse(localStorage.getItem("LOCALSTORAGE_COLLECTIONS")) || [],
  currentCollection: {},
  collectionsStack: [],
};

export const collectionsReducer = (state, action) => {
  if (action.type === "ADD_COLLECTION") {
    let updatedCollectionsList = state.collectionsList.concat(action.item);
    saveCollectionsToLocalStorage(updatedCollectionsList);
    return {
      ...state,
      collectionsList: updatedCollectionsList,
      currentCollection: action.item,
    };
  }

  if (action.type === "REMOVE_COLLECTION") {
    let updatedCollectionsList = state.collectionsList.filter(
      (collection) => collection.id !== action.id
    );
    saveCollectionsToLocalStorage(updatedCollectionsList);
    return {
      ...state,
      collectionsList: updatedCollectionsList,
      currentCollection: {},
    };
  }

  if (action.type === "ADJUST_COLLECTION") {
    let collectionsList = [...state.collectionsList];
    let targetCollectionIndex = collectionsList.findIndex(
      (collection) => collection.id === action.id
    );

    let collectionsStack = [...state.collectionsStack];
    let targetCollectionIndexInStack = collectionsStack.findIndex(
      (collection) => collection.id === action.id
    );
    if (targetCollectionIndexInStack !== -1) {
      collectionsStack[targetCollectionIndexInStack].name = action.value;
    }

    collectionsList[targetCollectionIndex].name = action.value;
    saveCollectionsToLocalStorage(collectionsList);
    return { ...state, collectionsList: collectionsList };
  }

  if (action.type === "SELECT_COLLECTION") {
    let updatedCollectionsStack = [];
    if (Object.keys(state.currentCollection).length !== 0) {
      updatedCollectionsStack = [...state.collectionsStack].concat(
        state.currentCollection
      );
    }

    let currentCollection = state.collectionsList.find(
      (collection) => collection.id === action.id
    );

    return {
      ...state,
      currentCollection: currentCollection ? currentCollection : {},
      collectionsStack: updatedCollectionsStack,
    };
  }

  if (action.type === "BACK_TO_PREV_COLLECTION") {
    if (state.collectionsStack.length <= 0) {
      return { ...state };
    }
    let currentCollection = state.collectionsStack.pop();
    return {
      ...state,
      currentCollection,
    };
  }

  if (action.type === "TOGGLE_TASK") {
    let collectionsList = [...state.collectionsList];
    let currentCollection = { ...state.currentCollection };
    let currentCollectionIndex = collectionsList.findIndex(
      (collection) => collection.id === currentCollection.id
    );
    let targetTaskIndex = currentCollection.tasksList.findIndex(
      (task) => task.id === action.id
    );

    currentCollection.tasksList[targetTaskIndex].isComplete =
      !currentCollection.tasksList[targetTaskIndex].isComplete;

    collectionsList[currentCollectionIndex] = currentCollection;

    saveCollectionsToLocalStorage(collectionsList);

    return { ...state, collectionsList, currentCollection };
  }

  if (action.type === "DELETE_TASK") {
    let collectionsList = [...state.collectionsList];
    let currentCollection = { ...state.currentCollection };
    let currentCollectionIndex = collectionsList.findIndex(
      (collection) => collection.id === currentCollection.id
    );
    currentCollection.tasksList = currentCollection.tasksList.filter(
      (task) => task.id !== action.id
    );
    collectionsList[currentCollectionIndex] = currentCollection;

    saveCollectionsToLocalStorage(collectionsList);

    return { ...state, collectionsList, currentCollection };
  }

  if (action.type === "ADJUST_TASK") {
    let collectionsList = [...state.collectionsList];
    let currentCollection = { ...state.currentCollection };
    let currentCollectionIndex = collectionsList.findIndex(
      (collection) => collection.id === currentCollection.id
    );
    let targetTaskIndex = currentCollection.tasksList.findIndex(
      (task) => task.id === action.id
    );

    currentCollection.tasksList[targetTaskIndex].content = action.value;
    collectionsList[currentCollectionIndex] = currentCollection;

    saveCollectionsToLocalStorage(collectionsList);

    return { ...state, collectionsList, currentCollection };
  }

  if (action.type === "ADD_TASK") {
    let collectionsList = [...state.collectionsList];
    let currentCollection = { ...state.currentCollection };
    let currentCollectionIndex = collectionsList.findIndex(
      (collection) => collection.id === currentCollection.id
    );

    currentCollection.tasksList.push(action.item);
    collectionsList[currentCollectionIndex] = currentCollection;

    saveCollectionsToLocalStorage(collectionsList);

    return { ...state, collectionsList, currentCollection };
  }

  return state;
};
