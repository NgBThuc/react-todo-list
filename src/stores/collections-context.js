import { createContext } from "react";

const CollectionsContext = createContext({
  collectionsList: [],
  currentCollection: {},
  collectionsStack: [],
  isCollectionShow: false,
  showCollection: () => {},
  hideCollection: () => {},
  addCollection: (collectionItem) => {},
  removeCollection: (collectionId) => {},
  adjustCollection: (collectionId, value) => {},
  selectCollection: (collectionId) => {},
  backToPrevCollection: () => {},
  toggleTask: (taskId) => {},
  deleteTask: (taskId) => {},
  adjustTask: (taskId, newValue) => {},
  addTask: (taskItem) => {},
});

export default CollectionsContext;
