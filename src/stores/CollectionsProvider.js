import { useReducer, useState } from "react";
import CollectionsContext from "./collections-context";
import {
  collectionsReducer,
  initialCollectionsState,
} from "./CollectionsReducer";

const CollectionsProvider = (props) => {
  const [isCollectionShow, setIsCollectionShow] = useState(false);
  const [collectionsState, dispatchCollectionsAction] = useReducer(
    collectionsReducer,
    initialCollectionsState
  );

  const showCollectionHandler = () => {
    setIsCollectionShow(true);
  };

  const hideCollectionHandler = () => {
    setIsCollectionShow(false);
  };

  const addCollectionHandler = (collectionItem) => {
    dispatchCollectionsAction({ type: "ADD_COLLECTION", item: collectionItem });
  };

  const removeCollectionHandler = (collectionId) => {
    dispatchCollectionsAction({ type: "REMOVE_COLLECTION", id: collectionId });
  };

  const adjustCollectionHandler = (collectionId, value) => {
    dispatchCollectionsAction({
      type: "ADJUST_COLLECTION",
      id: collectionId,
      value: value,
    });
  };

  const selectCollectionHandler = (collectionId) => {
    dispatchCollectionsAction({
      type: "SELECT_COLLECTION",
      id: collectionId,
    });
  };

  const backToPrevCollectionHandler = () => {
    dispatchCollectionsAction({
      type: "BACK_TO_PREV_COLLECTION",
    });
  };

  const toggleTaskHandler = (taskId) => {
    dispatchCollectionsAction({
      type: "TOGGLE_TASK",
      id: taskId,
    });
  };

  const addTaskHandler = (taskItem) => {
    dispatchCollectionsAction({
      type: "ADD_TASK",
      item: taskItem,
    });
  };

  const deleteTaskHandler = (taskId) => {
    dispatchCollectionsAction({
      type: "DELETE_TASK",
      id: taskId,
    });
  };

  const adjustTaskHandler = (taskId, newValue) => {
    dispatchCollectionsAction({
      type: "ADJUST_TASK",
      id: taskId,
      value: newValue,
    });
  };

  const collectionsContext = {
    collectionsList: collectionsState.collectionsList,
    currentCollection: collectionsState.currentCollection,
    collectionsStack: collectionsState.collectionsStack,
    isCollectionShow: isCollectionShow,
    showCollection: showCollectionHandler,
    hideCollection: hideCollectionHandler,
    addCollection: addCollectionHandler,
    removeCollection: removeCollectionHandler,
    adjustCollection: adjustCollectionHandler,
    selectCollection: selectCollectionHandler,
    backToPrevCollection: backToPrevCollectionHandler,
    toggleTask: toggleTaskHandler,
    deleteTask: deleteTaskHandler,
    adjustTask: adjustTaskHandler,
    addTask: addTaskHandler,
  };

  return (
    <CollectionsContext.Provider value={collectionsContext}>
      {props.children}
    </CollectionsContext.Provider>
  );
};

export default CollectionsProvider;
